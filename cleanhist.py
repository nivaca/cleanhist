#!/usr/bin/env python3.8
from pathlib import Path
import os.path
from os import remove
import re
#from pprint import pprint
from shutil import copyfile
from random import randrange

repatterns = [r"^kill ",
            r"^\d",
            r"^\.\.",
            r"^cd",
            r"^ls",
            r"^ll"
            r"^l $",
            r"^gd",
            r"^au",
            r"^yay",
            r"^yd ",
            r"^youtube",
            r"^grep",
            r"^mkdir",
            r"^rm ",
            r"^exit"]


home = str(Path.home())
histfile = ".zsh_history"
basepath = home + "/"
histpath = basepath + histfile

if not os.path.exists(histpath):
    print(f"{histpath} not found.")
    exit(0)


index = 1546041306

temp = []

with open(histpath, encoding="utf8", errors='ignore') as file:
    for l in file:
        temp.append(l.strip())

out = []

for l in temp:
    l = l[15:]  # strip the first 16 chars
    out.append(l)

temp = []

# eliminate duplicates
for l in out:
    if not (l in temp):
        temp.append(l)

out = []

for l in temp:
    inpat = False
    for pat in repatterns:
        if re.search(pat, l):
            inpat = True
    if (not l in out) and not inpat:
        out.append(l)

temp = out

out = []

for l in temp:
    entry = ": " + str(index) + ":0;" + l
    out.append(entry)
    index += 1


# make backup
bakpath = histpath + str(randrange(1000, 9999)) + ".bak"
if copyfile(histpath, bakpath):
    with open(histpath, "w") as file:
        for l in out:
            file.write(l)
            file.write("\n")


# pprint(out)
